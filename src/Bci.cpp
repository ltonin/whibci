#ifndef BCI_CPP
#define BCI_CPP

#include "Bci.hpp"

Bci::Bci(void) {};

Bci::~Bci(void) {};

int Bci::AddFrame(float * frame, unsigned int nsamples, unsigned int nchannels) {
	
	Eigen::MatrixXf cframe  = Eigen::MatrixXf::Zero(nchannels, nsamples);
	cframe = Eigen::Map<Eigen::MatrixXf>(frame, nchannels, nsamples);
	cframe.transposeInPlace();

	// Add block data to frame
	this->_buffer->Add(cframe.cast<double>());
};

bool Bci::IsReady(void) {
	return this->_buffer->IsFull();
};

#endif
