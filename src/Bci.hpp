#ifndef BCI_HPP
#define BCI_HPP

#include <Eigen/Dense>
#include <whicore/Buffer.hpp>
#include <whicore/Filter.hpp>
#include <whicore/Method.hpp>

class Bci {

	public:
		Bci(void);
		~Bci(void);
		
		virtual int AddFrame(float * frame, unsigned int nsamples, unsigned int nchannels);
		//virtual int Setup(const char* xmlfile);
		virtual int Run(void) = 0;
		virtual bool IsReady(void);
	
		//template<typename Derived>
		//void GetProbs(Eigen::MatrixBase<Derived> const & out_);
	public:

	protected:
		

		/* Input members */
		Buffer*		_buffer; 	// Take it as Input object to be implemented

	public: 
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

//template <typename Derived>
//void Bci::GetProbs(Eigen::MatrixBase<Derived> const & out_) {
//
//	Eigen::MatrixBase<Derived>& out = const_cast< Eigen::MatrixBase<Derived>& >(out_);
//	out.derived().resize(this->_probs.rows(), this->_probs.cols());
//	out = this->_probs;
//}

#endif
